﻿using Microsoft.AspNetCore.Mvc;
using ScoreBoard.Data;
using ScoreBoard.Views;

namespace ScoreBoard.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TeamController : ControllerBase
{

    private readonly RaceContext _context;

    public TeamController(RaceContext raceContext)
    {
        _context = raceContext;
    }
    
    [HttpGet]
    public IActionResult Get()
    {
        var allTeams = _context.Teams
            .Select(team => new TeamView(team, true))
            .ToList();
        return Ok(allTeams);
    }

    [HttpGet]
    [Route("{id:int}")]
    public IActionResult Get(int id)
    {
        var team = _context.Teams.Find(id);
        return team is null ? NotFound() : Ok(new TeamView(team, true));
    }
    
}