﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScoreBoard.Data;
using ScoreBoard.Views;

namespace ScoreBoard.Controllers;

[ApiController]
[Route("api/[controller]")]
public class SeasonController : ControllerBase
{

    private readonly RaceContext _context;

    public SeasonController(RaceContext raceContext)
    {
        _context = raceContext;
    }

    [HttpGet]
    public IActionResult Get()
    {
        var latestSeasons = _context.Seasons
            .OrderByDescending(s => s.Year)
            .Take(10)
            .Include(s => s.Races)
            .Select(s => new SeasonView(s).ToSimple())
            .ToList();
        return Ok(latestSeasons);
    }

    [HttpGet]
    [Route("{id:int}")]
    public IActionResult Get(int id)
    {
        var season = _context.Seasons.Find(id);
        return season is null ? NotFound() : Ok(new SeasonView(season));
    }

}