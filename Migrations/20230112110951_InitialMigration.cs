﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ScoreBoard.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Country = table.Column<string>(type: "TEXT", nullable: false),
                    City = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Seasons",
                columns: table => new
                {
                    Year = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seasons", x => x.Year);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Races",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<DateTime>(type: "TEXT", nullable: false),
                    SeasonID = table.Column<int>(type: "INTEGER", nullable: false),
                    LocationID = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Races", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Races_Locations_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Locations",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_Races_Seasons_SeasonID",
                        column: x => x.SeasonID,
                        principalTable: "Seasons",
                        principalColumn: "Year",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Drivers",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    TeamID = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Drivers_Teams_TeamID",
                        column: x => x.TeamID,
                        principalTable: "Teams",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RacePositions",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DriverID = table.Column<int>(type: "INTEGER", nullable: false),
                    RaceID = table.Column<int>(type: "INTEGER", nullable: true),
                    Position = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RacePositions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RacePositions_Drivers_DriverID",
                        column: x => x.DriverID,
                        principalTable: "Drivers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RacePositions_Races_RaceID",
                        column: x => x.RaceID,
                        principalTable: "Races",
                        principalColumn: "ID");
                });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 1, "Miami Gardens", "United States" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 2, "Jeddah", "Saudi Arabia" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 3, "Lusail", "Qatar" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 4, "Portimão", "Portugal" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 5, "Scarperia e San Piero", "Italy" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 6, "Baku", "Azerbaijan" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 7, "Sochi", "Russia" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 8, "Austin", "United States" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 9, "Greater Noida", "India" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 10, "Yeongam", "South Korea" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 11, "Abu Dhabi", "United Arab Emirates" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 12, "Marina Bay", "Singapore" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 13, "Valencia", "Spain" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 14, "Istanbul", "Turkey" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 15, "Shanghai", "China" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 16, "Sakhir", "Bahrain" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 17, "Sepang", "Malaysia" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 18, "Melbourne", "Australia" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 19, "Mimasaka", "Japan" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 20, "Castle Donington", "United Kingdom" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 21, "Montmeló", "Spain" });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "ID", "City", "Country" },
                values: new object[] { 22, "Magny-Cours", "France" });

            migrationBuilder.InsertData(
                table: "Seasons",
                column: "Year",
                value: 2000);

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 1, "", "Redbull" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 2, "", "Ferrari" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 3, "", "Mercedes" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 4, "", "Alpine" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 5, "", "McLaren" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 6, "", "Alfa Romeo" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 7, "", "Aston Martin" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 8, "", "Haas" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 9, "", "AlphaTauri" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[] { 10, "", "Williams" });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 1, "Max Verstappen", 1 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 2, "Sergio Perez", 1 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 3, "Charles Leclerc", 2 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 4, "Carlos Sainz", 2 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 5, "Lewis Hamilton", 3 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 6, "George Russell", 3 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 7, "Pierre Gasly", 4 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 8, "Esteban Ocon", 4 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 9, "Lando Norris", 5 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 10, "Oscar Piastri", 5 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 11, "Zhou Guanyu", 6 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 12, "Valtteri Bottas", 6 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 13, "Fernando Alonso", 7 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 14, "Lance Stroll", 7 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 15, "Kevin Magnussen", 8 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 16, "Nico Hulkenberg", 8 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 17, "Yuki Tsunoda", 9 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 18, "Nyck de Vries", 9 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 19, "Logan Sargeant", 10 });

            migrationBuilder.InsertData(
                table: "Drivers",
                columns: new[] { "ID", "Name", "TeamID" },
                values: new object[] { 20, "Alex Albon", 10 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 1, new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 2, new DateTime(2000, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 3, new DateTime(2000, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 4, new DateTime(2000, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 5, new DateTime(2000, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 6, new DateTime(2000, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 7, new DateTime(2000, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 7, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 8, new DateTime(2000, 4, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 8, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 9, new DateTime(2000, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 9, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 10, new DateTime(2000, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 10, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 11, new DateTime(2000, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 11, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 12, new DateTime(2000, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 12, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 13, new DateTime(2000, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 13, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 14, new DateTime(2000, 7, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 14, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 15, new DateTime(2000, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 16, new DateTime(2000, 8, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 16, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 17, new DateTime(2000, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 17, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 18, new DateTime(2000, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 18, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 19, new DateTime(2000, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 19, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 20, new DateTime(2000, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 21, new DateTime(2000, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, 2000 });

            migrationBuilder.InsertData(
                table: "Races",
                columns: new[] { "ID", "Date", "LocationID", "SeasonID" },
                values: new object[] { 22, new DateTime(2000, 11, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, 2000 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 1, 1, 1, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 2, 2, 2, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 3, 3, 3, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 4, 4, 4, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 5, 5, 5, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 6, 6, 6, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 7, 7, 7, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 8, 8, 8, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 9, 9, 9, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 10, 10, 10, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 11, 11, 11, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 12, 12, 12, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 13, 13, 13, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 14, 14, 14, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 15, 15, 15, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 16, 16, 16, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 17, 17, 17, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 18, 18, 18, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 19, 19, 19, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 20, 20, 20, 1 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 21, 1, 1, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 22, 2, 2, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 23, 3, 3, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 24, 4, 4, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 25, 5, 5, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 26, 6, 6, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 27, 7, 7, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 28, 8, 8, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 29, 9, 9, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 30, 10, 10, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 31, 11, 11, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 32, 12, 12, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 33, 13, 13, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 34, 14, 14, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 35, 15, 15, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 36, 16, 16, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 37, 17, 17, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 38, 18, 18, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 39, 19, 19, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 40, 20, 20, 2 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 41, 1, 1, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 42, 2, 2, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 43, 3, 3, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 44, 4, 4, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 45, 5, 5, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 46, 6, 6, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 47, 7, 7, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 48, 8, 8, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 49, 9, 9, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 50, 10, 10, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 51, 11, 11, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 52, 12, 12, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 53, 13, 13, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 54, 14, 14, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 55, 15, 15, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 56, 16, 16, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 57, 17, 17, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 58, 18, 18, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 59, 19, 19, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 60, 20, 20, 3 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 61, 1, 1, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 62, 2, 2, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 63, 3, 3, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 64, 4, 4, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 65, 5, 5, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 66, 6, 6, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 67, 7, 7, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 68, 8, 8, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 69, 9, 9, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 70, 10, 10, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 71, 11, 11, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 72, 12, 12, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 73, 13, 13, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 74, 14, 14, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 75, 15, 15, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 76, 16, 16, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 77, 17, 17, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 78, 18, 18, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 79, 19, 19, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 80, 20, 20, 4 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 81, 1, 1, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 82, 2, 2, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 83, 3, 3, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 84, 4, 4, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 85, 5, 5, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 86, 6, 6, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 87, 7, 7, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 88, 8, 8, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 89, 9, 9, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 90, 10, 10, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 91, 11, 11, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 92, 12, 12, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 93, 13, 13, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 94, 14, 14, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 95, 15, 15, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 96, 16, 16, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 97, 17, 17, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 98, 18, 18, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 99, 19, 19, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 100, 20, 20, 5 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 101, 1, 1, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 102, 2, 2, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 103, 3, 3, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 104, 4, 4, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 105, 5, 5, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 106, 6, 6, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 107, 7, 7, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 108, 8, 8, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 109, 9, 9, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 110, 10, 10, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 111, 11, 11, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 112, 12, 12, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 113, 13, 13, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 114, 14, 14, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 115, 15, 15, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 116, 16, 16, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 117, 17, 17, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 118, 18, 18, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 119, 19, 19, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 120, 20, 20, 6 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 121, 1, 1, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 122, 2, 2, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 123, 3, 3, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 124, 4, 4, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 125, 5, 5, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 126, 6, 6, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 127, 7, 7, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 128, 8, 8, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 129, 9, 9, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 130, 10, 10, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 131, 11, 11, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 132, 12, 12, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 133, 13, 13, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 134, 14, 14, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 135, 15, 15, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 136, 16, 16, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 137, 17, 17, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 138, 18, 18, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 139, 19, 19, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 140, 20, 20, 7 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 141, 1, 1, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 142, 2, 2, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 143, 3, 3, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 144, 4, 4, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 145, 5, 5, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 146, 6, 6, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 147, 7, 7, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 148, 8, 8, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 149, 9, 9, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 150, 10, 10, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 151, 11, 11, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 152, 12, 12, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 153, 13, 13, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 154, 14, 14, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 155, 15, 15, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 156, 16, 16, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 157, 17, 17, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 158, 18, 18, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 159, 19, 19, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 160, 20, 20, 8 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 161, 1, 1, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 162, 2, 2, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 163, 3, 3, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 164, 4, 4, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 165, 5, 5, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 166, 6, 6, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 167, 7, 7, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 168, 8, 8, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 169, 9, 9, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 170, 10, 10, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 171, 11, 11, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 172, 12, 12, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 173, 13, 13, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 174, 14, 14, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 175, 15, 15, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 176, 16, 16, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 177, 17, 17, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 178, 18, 18, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 179, 19, 19, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 180, 20, 20, 9 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 181, 1, 1, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 182, 2, 2, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 183, 3, 3, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 184, 4, 4, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 185, 5, 5, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 186, 6, 6, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 187, 7, 7, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 188, 8, 8, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 189, 9, 9, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 190, 10, 10, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 191, 11, 11, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 192, 12, 12, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 193, 13, 13, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 194, 14, 14, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 195, 15, 15, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 196, 16, 16, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 197, 17, 17, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 198, 18, 18, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 199, 19, 19, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 200, 20, 20, 10 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 201, 1, 1, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 202, 2, 2, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 203, 3, 3, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 204, 4, 4, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 205, 5, 5, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 206, 6, 6, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 207, 7, 7, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 208, 8, 8, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 209, 9, 9, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 210, 10, 10, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 211, 11, 11, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 212, 12, 12, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 213, 13, 13, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 214, 14, 14, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 215, 15, 15, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 216, 16, 16, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 217, 17, 17, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 218, 18, 18, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 219, 19, 19, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 220, 20, 20, 11 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 221, 1, 1, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 222, 2, 2, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 223, 3, 3, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 224, 4, 4, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 225, 5, 5, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 226, 6, 6, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 227, 7, 7, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 228, 8, 8, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 229, 9, 9, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 230, 10, 10, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 231, 11, 11, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 232, 12, 12, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 233, 13, 13, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 234, 14, 14, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 235, 15, 15, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 236, 16, 16, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 237, 17, 17, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 238, 18, 18, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 239, 19, 19, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 240, 20, 20, 12 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 241, 1, 1, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 242, 2, 2, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 243, 3, 3, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 244, 4, 4, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 245, 5, 5, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 246, 6, 6, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 247, 7, 7, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 248, 8, 8, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 249, 9, 9, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 250, 10, 10, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 251, 11, 11, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 252, 12, 12, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 253, 13, 13, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 254, 14, 14, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 255, 15, 15, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 256, 16, 16, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 257, 17, 17, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 258, 18, 18, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 259, 19, 19, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 260, 20, 20, 13 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 261, 1, 1, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 262, 2, 2, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 263, 3, 3, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 264, 4, 4, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 265, 5, 5, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 266, 6, 6, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 267, 7, 7, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 268, 8, 8, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 269, 9, 9, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 270, 10, 10, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 271, 11, 11, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 272, 12, 12, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 273, 13, 13, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 274, 14, 14, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 275, 15, 15, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 276, 16, 16, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 277, 17, 17, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 278, 18, 18, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 279, 19, 19, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 280, 20, 20, 14 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 281, 1, 1, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 282, 2, 2, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 283, 3, 3, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 284, 4, 4, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 285, 5, 5, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 286, 6, 6, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 287, 7, 7, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 288, 8, 8, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 289, 9, 9, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 290, 10, 10, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 291, 11, 11, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 292, 12, 12, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 293, 13, 13, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 294, 14, 14, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 295, 15, 15, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 296, 16, 16, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 297, 17, 17, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 298, 18, 18, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 299, 19, 19, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 300, 20, 20, 15 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 301, 1, 1, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 302, 2, 2, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 303, 3, 3, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 304, 4, 4, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 305, 5, 5, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 306, 6, 6, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 307, 7, 7, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 308, 8, 8, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 309, 9, 9, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 310, 10, 10, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 311, 11, 11, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 312, 12, 12, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 313, 13, 13, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 314, 14, 14, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 315, 15, 15, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 316, 16, 16, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 317, 17, 17, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 318, 18, 18, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 319, 19, 19, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 320, 20, 20, 16 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 321, 1, 1, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 322, 2, 2, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 323, 3, 3, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 324, 4, 4, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 325, 5, 5, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 326, 6, 6, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 327, 7, 7, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 328, 8, 8, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 329, 9, 9, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 330, 10, 10, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 331, 11, 11, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 332, 12, 12, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 333, 13, 13, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 334, 14, 14, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 335, 15, 15, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 336, 16, 16, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 337, 17, 17, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 338, 18, 18, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 339, 19, 19, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 340, 20, 20, 17 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 341, 1, 1, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 342, 2, 2, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 343, 3, 3, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 344, 4, 4, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 345, 5, 5, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 346, 6, 6, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 347, 7, 7, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 348, 8, 8, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 349, 9, 9, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 350, 10, 10, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 351, 11, 11, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 352, 12, 12, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 353, 13, 13, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 354, 14, 14, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 355, 15, 15, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 356, 16, 16, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 357, 17, 17, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 358, 18, 18, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 359, 19, 19, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 360, 20, 20, 18 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 361, 1, 1, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 362, 2, 2, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 363, 3, 3, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 364, 4, 4, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 365, 5, 5, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 366, 6, 6, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 367, 7, 7, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 368, 8, 8, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 369, 9, 9, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 370, 10, 10, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 371, 11, 11, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 372, 12, 12, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 373, 13, 13, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 374, 14, 14, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 375, 15, 15, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 376, 16, 16, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 377, 17, 17, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 378, 18, 18, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 379, 19, 19, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 380, 20, 20, 19 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 381, 1, 1, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 382, 2, 2, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 383, 3, 3, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 384, 4, 4, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 385, 5, 5, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 386, 6, 6, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 387, 7, 7, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 388, 8, 8, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 389, 9, 9, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 390, 10, 10, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 391, 11, 11, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 392, 12, 12, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 393, 13, 13, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 394, 14, 14, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 395, 15, 15, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 396, 16, 16, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 397, 17, 17, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 398, 18, 18, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 399, 19, 19, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 400, 20, 20, 20 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 401, 1, 1, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 402, 2, 2, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 403, 3, 3, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 404, 4, 4, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 405, 5, 5, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 406, 6, 6, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 407, 7, 7, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 408, 8, 8, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 409, 9, 9, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 410, 10, 10, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 411, 11, 11, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 412, 12, 12, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 413, 13, 13, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 414, 14, 14, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 415, 15, 15, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 416, 16, 16, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 417, 17, 17, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 418, 18, 18, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 419, 19, 19, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 420, 20, 20, 21 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 421, 1, 1, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 422, 2, 2, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 423, 3, 3, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 424, 4, 4, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 425, 5, 5, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 426, 6, 6, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 427, 7, 7, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 428, 8, 8, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 429, 9, 9, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 430, 10, 10, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 431, 11, 11, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 432, 12, 12, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 433, 13, 13, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 434, 14, 14, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 435, 15, 15, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 436, 16, 16, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 437, 17, 17, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 438, 18, 18, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 439, 19, 19, 22 });

            migrationBuilder.InsertData(
                table: "RacePositions",
                columns: new[] { "ID", "DriverID", "Position", "RaceID" },
                values: new object[] { 440, 20, 20, 22 });

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_TeamID",
                table: "Drivers",
                column: "TeamID");

            migrationBuilder.CreateIndex(
                name: "IX_RacePositions_DriverID",
                table: "RacePositions",
                column: "DriverID");

            migrationBuilder.CreateIndex(
                name: "IX_RacePositions_RaceID",
                table: "RacePositions",
                column: "RaceID");

            migrationBuilder.CreateIndex(
                name: "IX_Races_LocationID",
                table: "Races",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_Races_SeasonID",
                table: "Races",
                column: "SeasonID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RacePositions");

            migrationBuilder.DropTable(
                name: "Drivers");

            migrationBuilder.DropTable(
                name: "Races");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Seasons");
        }
    }
}
