import React, {Component, useEffect, useState} from 'react';
import TwoThirds from "./TwoThirds";
import './two-thirds.css'
import {Season as SeasonType} from "./response-types";
import Season from "./Season";
import {Link} from "react-router-dom";

const getSeasons = async (): Promise<SeasonType[]> => {
    const response = await fetch('/api/season')
    if (!response.ok) throw new Error()
    return await response.json()
}

export default function Home() {
    const [seasons, setSeasons] = useState<SeasonType[]>([])
    useEffect(() => {
        getSeasons().then(data => setSeasons(data))
    }, [])
    return <>
        <h1 className="title">
            Season Scores
        </h1>
        <TwoThirds>
            <div className="left">
                {seasons.map(s => <Season key={s.year} {...s} />)}
            </div>
            <div className="right">
                <h3>About</h3>
                <p>
                    This is a simple web-app for displaying scores from F1 Racing seasons.
                    It's written in C# for ASP.NET Core MVC, and JS/TS for the React frontend.
                </p>
                <p>
                    For more information, see the 
                    public <Link to="https://gitlab.com/hgwwinchester/scoreboard">GitLab repo</Link>.
                </p>
            </div>
        </TwoThirds>
    </>
}