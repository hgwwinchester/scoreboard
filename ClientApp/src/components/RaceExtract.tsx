﻿import {Race} from "./response-types";
import './race.css'

export default function RaceExtract(props: Race) {
    const date = new Date(props.date).toLocaleDateString()
    return <div className="race-extract">
        <div className="race-info">
            <p className="race-location">{props.location?.city}, {props.location?.country}</p>
            <p className="race-date">{date}</p>
        </div>
        <div className="race-winners">
            {
                props.positions
                    .sort((a, b) => a.position - b.position)
                    .slice(0, 3)
                    .map(p => {
                        return <div key={p.position} className="race-winner">
                            <p>{p.position}</p>
                            <p>{p.driver?.name}</p>
                            <p>{p.driver?.team?.name}</p>
                        </div>
                    })
            }
        </div>
    </div>
}