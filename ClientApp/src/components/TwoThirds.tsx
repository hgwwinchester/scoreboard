﻿import {ReactNode} from "react";
import './two-thirds.css'

type TwoThirdsProps = {
    children?: ReactNode
}

export default function TwoThirds({children}: TwoThirdsProps) {
    return <section className="two-thirds">
        {children}
    </section>
}