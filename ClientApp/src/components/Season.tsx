﻿import {Season as SeasonType} from "./response-types";
import './season.css';
import {Link} from "react-router-dom";
import SeasonDriver from "./SeasonDriver";

export default function Season(props: SeasonType) {
    const drivers = [...props.drivers]
    drivers.sort((a, b)=> b.seasonScore - a.seasonScore)
    return <div className="season-extract">
        <div className="season-header">
            <h3 className="season-title">Season Y{props.year}</h3>
            <Link className="season-link" to={`/seasons/${props.year}`}>Detailed View</Link>
        </div>
        <div className="season-drivers">
            {drivers.map(d => {
                return <SeasonDriver key={d.name} {...d} />
            })}
        </div>
    </div>
}