import './nav.css'
import React from "react";
import {Link} from "react-router-dom";

type NavMenuProps = {
    links: {
        name: string
        href: string
    }[]
}

export default function NavMenu({links}: NavMenuProps) {
    return <nav>
        <p className="nav-title">Season Scores</p>
        <ul className="nav-links">
            {links.map(l => {
                return <li key={l.href}><Link className="nav-link" to={l.href}>{l.name}</Link></li>
            })}
        </ul>
    </nav>
}