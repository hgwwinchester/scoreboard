﻿import {Team as TeamType} from "./response-types";
import {useParams} from "react-router";
import React, {useEffect, useState} from "react";
import TwoThirds from "./TwoThirds";
import Team from "./Team";
import './teams.css';

const getSeason = async (year: string): Promise<TeamType[]> => {
    const response = await fetch(`/api/team`)
    if (!response.ok) throw new Error()
    return await response.json()
}

export default function TeamPage() {
    const { year } = useParams()
    const [teams, setTeams] = useState<TeamType[]>([])
    useEffect(() => {
        getSeason(year ?? '1').then(data => {
            setTeams(data)
            console.log(data)
        })
    }, [])
    return <>
        <h1 className="title">Teams</h1>
        <TwoThirds>
            <div className="left">
                <div className="teams">
                    {teams.map(t => {
                        return <Team key={t.name} {...t} />
                    })}
                </div>
            </div>
            <div className="right">
                <h2>About</h2>
                <p>There are 10 teams with 2 drivers each, for 20 drivers in total.</p>
            </div>
        </TwoThirds>
    </>
}