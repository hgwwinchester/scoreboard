﻿import TwoThirds from "./TwoThirds";
import React, {useEffect, useState} from "react";
import {Season} from "./response-types";
import {useParams} from "react-router";
import SeasonFull from "./SeasonFull";
import RaceExtract from "./RaceExtract";

const getSeason = async (year: string): Promise<Season> => {
    const response = await fetch(`/api/season/${year}`)
    if (!response.ok) throw new Error()
    return await response.json()
}

const initSeason: Season = {
    year: 0,
    drivers: [],
    races: []
}

export default function SeasonPage() {
    const { year } = useParams()
    const [season, setSeason] = useState<Season>(initSeason)
    useEffect(() => {
        getSeason(year ?? '1').then(data => {
            setSeason(data)
            console.log(data)
        })
    }, [])
    return <>
        <h1 className="title">Season Y{season.year}</h1>
        <TwoThirds>
            <div className="left">
                <SeasonFull {...season} />
            </div>
            <div className="right">
                <h2>Races</h2>
                {season.races?.map(r => {
                    return <RaceExtract key={r.date} {...r} />
                })}
            </div>
    </TwoThirds>
    </>
}