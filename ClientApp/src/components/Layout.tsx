import React, {Component, ReactNode} from 'react';
import NavMenu from "./NavMenu";

export default function Layout({children}: {children: ReactNode}) {
    return <>
        <NavMenu links={[
            {name: "Seasons", href: "/"},
            {name: "Teams", href: "/teams"}
        ]} />
        {children}
    </>
}