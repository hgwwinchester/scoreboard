﻿export type Season = {
    year: number
    drivers: DriverSeason[]
    races?: Race[]
}

export type Driver = {
    name: string
    team?: Team
}

export type DriverSeason = {
    seasonScore: number
} & Driver

export type Team = {
    name: string
    description: string
    drivers?: Driver[]
}

export type Location = {
    country: string,
    city: string
}

export type Race = {
    date: string
    season?: Season
    location?: Location
    positions: RacePosition[]
}

export type RacePosition = {
    position: number
    race?: Race
    driver?: Driver
}