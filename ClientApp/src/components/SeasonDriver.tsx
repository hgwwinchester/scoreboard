﻿import {DriverSeason} from "./response-types";
import './season.css';

export default function SeasonDriver({name, team, seasonScore}: DriverSeason) {
    return <div className="season-driver">
        <h4 className="season-driver-title">{name}</h4>
        <div className="season-driver-info">
            {team && <p className="season-driver-info-team">{team.name}</p>}
            <p>/</p>
            <p className="season-driver-info-score">{seasonScore} Points</p>
        </div>
    </div>
}