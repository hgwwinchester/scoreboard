﻿import {Team as TeamType} from "./response-types";

export default function Team(props: TeamType) {
    return <div className="team">
        <h3 className="team-name">{props.name}</h3>
        <p className="team-desc">{props.description}</p>
        <div className="team-drivers">
            {props.drivers?.map(d => {
                return <p>{d.name}</p>
            })}
        </div>
    </div>
}