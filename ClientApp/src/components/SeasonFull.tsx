﻿import {Season} from "./response-types";
import './season.css';
import {Link} from "react-router-dom";
import SeasonDriver from "./SeasonDriver";

export default function SeasonFull(props: Season) {
    const drivers = [...props.drivers]
    drivers.sort((a, b)=> b.seasonScore - a.seasonScore)
    return <div className="season-full">
        <div className="season-drivers">
            {drivers.map(d => {
                return <SeasonDriver key={d.name} {...d} />
            })}
        </div>
    </div>
}