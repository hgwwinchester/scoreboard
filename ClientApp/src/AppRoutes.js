import Home from "./components/Home";
import SeasonPage from "./components/SeasonPage";
import TeamPage from "./components/TeamPage";

const AppRoutes = [
  {
    index: true,
    element: <Home />
  },
  {
    path: "/seasons/:year",
    element: <SeasonPage />
  },
  {
    path: '/teams',
    element: <TeamPage />
  }
];

export default AppRoutes;
