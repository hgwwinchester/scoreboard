﻿# Score Board

A single-page React/ASP app for displaying randomised scores for
racing seasons. It's super rough (the DB makes ~200 queries for some simple things)
and messy (the UI is... :|). But it works, more or less.

For the sake of time, there's no documentation, database operations aren't optimised
(some API calls perform hundreds), and the React frontend has _zero_ structure
(e.g. I'd usually use a mix of CSS and StyledJSX for handling theming).

The app uses an SQLite database which is created using the 'dotnet-ef database update' command.
This process isn't automated during building. The DB ConnectionString can be set as a secret or
in the applicationsettings.json. A production ready build - which this is far from - might use
AWS Secrets Manager, or something similar.

Once a database is created, the project can be built and connected to. Deploying the app might
require placing it within a fully featured webserver, like nginx.

This app has dependencies on Node and the .NET runtime.

## Scoring

Scores are as follows:

| Position | Score |
|----------|-------|
| 1        | 25    |
| 2        | 18    |
| 3        | 15    |
| 4        | 12    |
| 5        | 10    |
| 6        | 8     |
| 7        | 6     |
| 8        | 4     |
| 9        | 2     |
| 10       | 1     |
| 11       | 0     |

There are 22 Grands Prix in a season, and 10 teams, each with 2 drivers.
Racers may only have one position
in a race. Final positions should be found by sorting the random values in descending
order, and taking the indexes.
Final season scores are found as the sum of race scores, with the lowest
three removed.

## API Requirements

An API should:
- Return the latest seasons races, and scores.
- Return a profile for each racer, with the 10 latest seasons scores,
  and their final scores.

## View

SPA React app. Views:
 - Season
   - Latest 10 simple (top 3 winner, w/ scores)
   - Latest overview (on home, all team positions, total scores, and race overviews)
 - Team
   - List of 10 teams, each with two drivers
   - Individual team profile, and history (last 10)

## Database

The EF CLI tools (dotnet-ef) are installed locally (in the project root directory).

```shell
dotnet new tool-manifest
dotnet tool install dotnet-ef
```

On checkout, tools can be installed/restored with:

```shell
dotnet tool restore
```

To use it, run:

```shell
dotnet tool run dotnet-ef
```

Database requires a connection string, for "RaceDB".
When developing (local), can use .NET user secrets, e.g.

```shell
dotnet user-secrets set ConnectionStrings:RaceDB "Data Source=C:\Users\xxx\xxx\race.db"
```

Update the database to the latest migration with

```shell
dotnet tool run dotnet-ef database update
```

Or create a new migration (following a change) with

```shell
dotnet tool run dotnet-ef migrations add MigrationName
```