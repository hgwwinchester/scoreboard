﻿namespace ScoreBoard.Model.Team;

public class Team
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    
    public virtual ICollection<Driver> Drivers { get; set; }
}