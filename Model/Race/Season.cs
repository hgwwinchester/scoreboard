﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScoreBoard.Model.Race;

public class Season
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int Year { get; set; }
    
    public virtual ICollection<Race> Races { get; set; }
}