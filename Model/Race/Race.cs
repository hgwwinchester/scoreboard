﻿using System.ComponentModel.DataAnnotations;

namespace ScoreBoard.Model.Race;

public class Race
{
    
    public int ID { get; set; }
    
    [DataType(DataType.Date)]
    public DateTime Date { get; set; }
    
    // Delete race if season is deleted
    public int SeasonID { get; set; }
    public virtual Season Season { get; set; }
    
    // Location is nullable so race information persists for data if Location is deleted
    public int? LocationID { get; set; }
    public virtual Location Location { get; set; }
    
    public virtual ICollection<RacePosition> Positions { get; set; }
    
}