﻿namespace ScoreBoard.Model.Race;

public static class RaceScore
{

    private static readonly int[] _scoreTable = new int[10]{
        25,
        18,
        15,
        12,
        10,
        8,
        6,
        4,
        2,
        1
    };

    public static int PositionToScore(int position)
    {
        return position is > 10 or < 1 ? 0 : _scoreTable[position - 1];
    }

    public static int SeasonTotalScore(IEnumerable<int> positions)
    {
        return positions.ToList()
            .OrderBy(p => p)
            .Skip(3)
            .Select(PositionToScore)
            .Sum();
    }
    
}