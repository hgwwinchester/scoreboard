﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScoreBoard.Model.Race;
using Team;

public class RacePosition
{
    public int ID { get; set; }
    
    public int DriverID { get; set; }
    public virtual Driver Driver { get; set; }
    
    // Don't want to lose a teams history if races are deleted
    public int? RaceID { get; set; }
    public virtual Race Race { get; set; }
    
    public int Position { get; set; }

}