﻿using ScoreBoard.Model.Race;

namespace ScoreBoard.Views;

public class RaceView
{
    public DateTime Date { get; set; }
    public SeasonView? Season { get; set; }
    public LocationView Location { get; set; }
    public ICollection<RacePositionView> Positions { get; set; }

    public RaceView(Race race, bool getSeason = false)
    {
        Date = race.Date;
        if (getSeason) Season = new SeasonView(race.Season);
        Location = new LocationView(race.Location);
        Positions = new List<RacePositionView>(
            race.Positions
                .Select(p => new RacePositionView(p, RacePositionViewPopulation.Driver))
                .OrderBy(p => p.Position)
            );
    }
    
}