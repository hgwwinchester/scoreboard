﻿using ScoreBoard.Model.Race;
using ScoreBoard.Model.Team;

namespace ScoreBoard.Views;

public class SeasonView
{
    
    public int Year { get; set; }
    public ICollection<RaceView> Races { get; set; }
    
    public ICollection<DriverSeasonView> Drivers { get; set; }

    public SeasonView(Season season)
    {
        Year = season.Year;
        Races = new List<RaceView>(
            season.Races
                .Select(r => new RaceView(r))
        );
        // Restructure, so more detailed team info at top, along with season scores
        var scores = season.Races
            .Select(r => r.Positions)
            .SelectMany(ps => ps)
            .GroupBy(p => p.Driver)
            .Select(g => 
                new DriverSeasonView(g.Key, RaceScore.SeasonTotalScore(g.Select(p => p.Position)), true));

        Drivers = scores.ToList();
    }

    public SimpleSeasonView ToSimple()
    {
        return new SimpleSeasonView()
        {
            Year = Year,
            Drivers = Drivers
        };
    }

}