﻿using ScoreBoard.Model.Race;

namespace ScoreBoard.Views;

public class LocationView
{
    public string Country { get; set; }
    public string City { get; set; }

    public LocationView(Location location)
    {
        Country = location.Country;
        City = location.City;
    }
}