﻿using ScoreBoard.Model.Team;

namespace ScoreBoard.Views;

public class DriverView
{
    public string Name { get; set; }
    public TeamView? Team { get; set; }

    public DriverView(Driver driver, bool getTeam = false)
    {
        Name = driver.Name;
        if (getTeam) Team = new TeamView(driver.Team);
    }
}