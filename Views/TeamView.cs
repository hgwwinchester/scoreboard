﻿using ScoreBoard.Model.Team;

namespace ScoreBoard.Views;

public class TeamView
{
    public string Name { get; set; }
    public string Description { get; set; }
    public ICollection<DriverView>? Drivers { get; set; }

    public TeamView(Team team, bool getDrivers = false)
    {
        Name = team.Name;
        Description = team.Description;
        if (getDrivers) Drivers = new List<DriverView>(
            team.Drivers
                .Select(d => new DriverView(d))
            );
    }
}