﻿using ScoreBoard.Model.Team;

namespace ScoreBoard.Views;

public class DriverSeasonView : DriverView
{
    
    public int SeasonScore { get; set; }
    
    public DriverSeasonView(Driver driver, int seasonScore, bool getTeam = false) : base(driver, getTeam)
    {
        SeasonScore = seasonScore;
    }
}