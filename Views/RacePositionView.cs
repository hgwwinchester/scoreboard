﻿using ScoreBoard.Model.Race;

namespace ScoreBoard.Views;

public enum RacePositionViewPopulation
{
    None,
    Race,
    Driver
}

public class RacePositionView
{
    public RaceView? Race { get; set; }
    public DriverView? Driver { get; set; }
    public int Position { get; set; }
    
    public int Score { get; set; }

    public RacePositionView(RacePosition racePosition, RacePositionViewPopulation populate)
    {
        Position = racePosition.Position;
        Score = RaceScore.PositionToScore(racePosition.Position);
        if (populate == RacePositionViewPopulation.Driver) Driver = new DriverView(racePosition.Driver, true);
        if (populate == RacePositionViewPopulation.Race) Race = new RaceView(racePosition.Race);
    }
}