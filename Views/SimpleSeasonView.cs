﻿namespace ScoreBoard.Views;

public class SimpleSeasonView
{
    public int Year { get; set; }

    public ICollection<DriverSeasonView> Drivers { get; set; }
    
}