﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using ScoreBoard.Model.Race;
using ScoreBoard.Model.Team;

namespace ScoreBoard.Data;

public static class SeedRaceData
{
    
    // TODO pull seed data from JSON config files
    
    public static void Seed(this ModelBuilder modelBuilder)
    {
        SeedTeams(modelBuilder);
        SeedDrivers(modelBuilder);
        SeedLocations(modelBuilder);
        SeedSeasons(modelBuilder);
        SeedRaces(modelBuilder);
        SeedRacePositions(modelBuilder);
    }

    private static void SeedTeams(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Team>().HasData(
            new Team {ID = 1, Name = "Redbull", Description = "The reddest of bulls."},    
            new Team {ID = 2, Name = "Ferrari", Description = ""},    
            new Team {ID = 3, Name = "Mercedes", Description = ""},    
            new Team {ID = 4, Name = "Alpine", Description = ""},    
            new Team {ID = 5, Name = "McLaren", Description = ""},    
            new Team {ID = 6, Name = "Alfa Romeo", Description = ""},    
            new Team {ID = 7, Name = "Aston Martin", Description = ""},    
            new Team {ID = 8, Name = "Haas", Description = ""},    
            new Team {ID = 9, Name = "AlphaTauri", Description = ""},    
            new Team {ID = 10, Name = "Williams", Description = ""}    
        );
    }

    private static void SeedDrivers(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Driver>().HasData(
            new Driver {ID = 1, Name = "Max Verstappen", TeamID = 1},
            new Driver {ID = 2, Name = "Sergio Perez", TeamID = 1},
            new Driver {ID = 3, Name = "Charles Leclerc", TeamID = 2},
            new Driver {ID = 4, Name = "Carlos Sainz", TeamID = 2},
            new Driver {ID = 5, Name = "Lewis Hamilton", TeamID = 3},
            new Driver {ID = 6, Name = "George Russell", TeamID = 3},
            new Driver {ID = 7, Name = "Pierre Gasly", TeamID = 4},
            new Driver {ID = 8, Name = "Esteban Ocon", TeamID = 4},
            new Driver {ID = 9, Name = "Lando Norris", TeamID = 5},
            new Driver {ID = 10, Name = "Oscar Piastri", TeamID = 5},
            new Driver {ID = 11, Name = "Zhou Guanyu", TeamID = 6},
            new Driver {ID = 12, Name = "Valtteri Bottas", TeamID = 6},
            new Driver {ID = 13, Name = "Fernando Alonso", TeamID = 7},
            new Driver {ID = 14, Name = "Lance Stroll", TeamID = 7},
            new Driver {ID = 15, Name = "Kevin Magnussen", TeamID = 8},
            new Driver {ID = 16, Name = "Nico Hulkenberg", TeamID = 8},
            new Driver {ID = 17, Name = "Yuki Tsunoda", TeamID = 9},
            new Driver {ID = 18, Name = "Nyck de Vries", TeamID = 9},
            new Driver {ID = 19, Name = "Logan Sargeant", TeamID = 10},
            new Driver {ID = 20, Name = "Alex Albon", TeamID = 10}
        );
    }

    private static void SeedLocations(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Location>().HasData(
            new Location {ID = 1, Country = "United States", City = "Miami Gardens"},
            new Location {ID = 2, Country = "Saudi Arabia", City = "Jeddah"},
            new Location {ID = 3, Country = "Qatar", City = "Lusail"},
            new Location {ID = 4, Country = "Portugal", City = "Portimão"},
            new Location {ID = 5, Country = "Italy", City = "Scarperia e San Piero"},
            new Location {ID = 6, Country = "Azerbaijan", City = "Baku"},
            new Location {ID = 7, Country = "Russia", City = "Sochi"},
            new Location {ID = 8, Country = "United States", City = "Austin"},
            new Location {ID = 9, Country = "India", City = "Greater Noida"},
            new Location {ID = 10, Country = "South Korea", City = "Yeongam"},
            new Location {ID = 11, Country = "United Arab Emirates", City = "Abu Dhabi"},
            new Location {ID = 12, Country = "Singapore", City = "Marina Bay"},
            new Location {ID = 13, Country = "Spain", City = "Valencia"},
            new Location {ID = 14, Country = "Turkey", City = "Istanbul"},
            new Location {ID = 15, Country = "China", City = "Shanghai"},
            new Location {ID = 16, Country = "Bahrain", City = "Sakhir"},
            new Location {ID = 17, Country = "Malaysia", City = "Sepang"},
            new Location {ID = 18, Country = "Australia", City = "Melbourne"},
            new Location {ID = 19, Country = "Japan", City = "Mimasaka"},
            new Location {ID = 20, Country = "United Kingdom", City = "Castle Donington"},
            new Location {ID = 21, Country = "Spain", City = "Montmeló"},
            new Location {ID = 22, Country = "France", City = "Magny-Cours"}
        );
    }

    private static void SeedSeasons(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Season>().HasData(
            new Season {Year = 2000}
        );
    }

    private static void SeedRaces(ModelBuilder modelBuilder)
    {
        var races = new int[22].ToList().Select((_, i) => new Race
        {
            ID = i + 1,
            Date = new DateTime(2000, (i / 2) + 1, i % 2 <= 0 ? 1 : 15),
            LocationID = i + 1,
            SeasonID = 2000
        });
        modelBuilder.Entity<Race>().HasData(races);
    }

    private static void SeedRacePositions(ModelBuilder modelBuilder)
    {
        // 22 races and 20 drivers, so 22*20 positions
        var racePositions = new int[22 * 20].ToList().Select((_, i) => new RacePosition
        {
            ID = i+1,
            DriverID = (i%20)+1,
            RaceID = (i/20)+1,
            Position = (i%20)+1
        });
        modelBuilder.Entity<RacePosition>().HasData(racePositions);
    }
}