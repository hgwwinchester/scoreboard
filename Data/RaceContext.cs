﻿using Microsoft.EntityFrameworkCore;
using ScoreBoard.Model.Race;
using ScoreBoard.Model.Team;

namespace ScoreBoard.Data;

public class RaceContext : DbContext
{
    public DbSet<Season> Seasons { get; set; }
    public DbSet<Location> Locations { get; set; }
    public DbSet<Race> Races { get; set; }
    public DbSet<Team> Teams { get; set; }
    public DbSet<Driver> Drivers { get; set; }
    public DbSet<RacePosition> RacePositions { get; set; }

    public RaceContext(DbContextOptions<RaceContext> options) : base(options)
    {
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Seed();
    }
    
}